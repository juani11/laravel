<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePizza;
use App\Repositories\Pizza\IPizzaRepository;
use Illuminate\Http\Request;

use stdClass;

class PizzaController extends Controller
{

    /**
     * repositorio de pizzas. Controlador depende ahora de una interfaz y se abstrae de la implementacion
     */
    protected $pizzas;

    public function __construct(IPizzaRepository $pizzaRep)
    {
        $this->pizzas = $pizzaRep;
    }


    public function index()
    {
        $pizzas = $this->pizzas->all();
        return view(
            'pizzas.index',
            ['pizzas' => $pizzas]
        );
    }

    public function show($id)
    {

        $pizza = $this->pizzas->findById($id);

        return view('pizzas.show', ['pizza' => $pizza]);
    }

    public function create()
    {
        return view('pizzas.create');
    }


    //Controller Action para /pizzas POST
    public function store(StorePizza $request)
    {

        if ($request->validated()) {


            $newPizza = new stdClass();

            $newPizza->name = request('name');
            $newPizza->type = request('type');
            $newPizza->base = request('base');
            $newPizza->price = request('price');
            $newPizza->toppings = request('toppings');

            $this->pizzas->save($newPizza);

            return redirect('/')->with('mssg', 'Orden realizada');
        }
    }


    //Controller Action para /pizzas/{id} DELETE
    public function destroy($id)
    {
        $this->pizzas->deleteById($id);

        return redirect('/pizzas');
    }

    public function edit($id)
    {
        $pizza = $this->pizzas->findById($id);

        return view('pizzas.edit', ['pizza' => $pizza]);
    }


    public function update(StorePizza $request, $id)
    {
        if ($request->validated()) {

            $pizzaEdit = new stdClass();

            $pizzaEdit->name = request('name');
            $pizzaEdit->type = request('type');
            $pizzaEdit->base = request('base');
            $pizzaEdit->price = request('price');
            $pizzaEdit->toppings = request('toppings');

            $this->pizzas->update($id, $pizzaEdit);

            return redirect('/')->with('mssg', 'Orden actualizada');
        }
    }
}
