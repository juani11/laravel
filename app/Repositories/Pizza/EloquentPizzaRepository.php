<?php

namespace App\Repositories\Pizza;

use App\Repositories\Pizza\IPizzaRepository;

use App\Pizza; //importo el modelo Eloquent de Pizza , para interactuar con la bd


class EloquentPizzaRepository implements IPizzaRepository
{
    public function all()
    {
        //LLamo al modelo para obtener las pizzas de la bd
        $pizzas = Pizza::all();
        //$pizzas = Pizza::orderBy('name')->get()
        //$pizzas = Pizza::where('type','hawaiian')->get()
        //$pizzas = Pizza::latest()->get()
        return $pizzas;
    }

    public function save($pizza)
    {
        $newPizza = new Pizza();

        $newPizza->name = $pizza->name;
        $newPizza->type =  $pizza->type;
        $newPizza->base = $pizza->base;
        $newPizza->price =  $pizza->price;
        $newPizza->toppings =  $pizza->toppings;

        $newPizza->save();
    }

    public function findById($pizzaId)
    {
        //LLamo al modelo para obtener una pizza especifica por id
        $pizza = Pizza::findOrFail($pizzaId);
        return $pizza;
    }

    public function deleteById($pizzaId)
    {
        //LLamo al modelo para obtener la pizza a borrar por id
        $pizza = $this->findById($pizzaId);

        $pizza->delete();
    }

    public function update($pizzaId, $data)
    {
        $pizza = $this->findById($pizzaId);

        $pizza->name = $data->name;
        $pizza->type = $data->type;
        $pizza->base = $data->base;
        $pizza->price = $data->price;
        $pizza->toppings = $data->toppings;
        $pizza->save();
    }
}
