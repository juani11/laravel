<?php

namespace App\Repositories\Pizza;

use App\Repositories\Pizza\IPizzaRepository;

//Esta clase no tiene mucho sentido. 
//Unicamente para tener otra implementacion de la interfaz Pizza Repository y ver si funciona el bindeo de otra clase con la interfaz, en el AppServiceProvider.
class HardPizzaRepository implements IPizzaRepository
{

    private $hardPizzas;

    public function __construct()
    {

        $this->hardPizzas = [
            [
                'id' => 1,
                'name' => 'Hard Name',
                'type' =>  "Moozzarella ",
                'base' =>  "Salsa de tomate ",
                'price' => 120,
                'toppings' => [
                    "musshrooms ",
                    "peppers"
                ]
            ],
            [
                'id' => 2,
                'name' => 'Hard Name 2',
                'type' =>  "Napolitana ",
                'base' =>  "Salsa de tomate  ",
                'price' => 160,
                'toppings' => [
                    "garlics ",
                    "olivs"
                ]
            ]
        ];
    }

    public function setHardPizzas($hardPizzas)
    {
        $this->hardPizzas = $hardPizzas;
    }


    public function all()
    {
        return json_decode(json_encode($this->hardPizzas));
    }

    public function findById($pizzaId)
    {
        foreach ($this->hardPizzas as $pizza) {
            if ($pizzaId == $pizza['id']) {
                return json_decode(json_encode($pizza));
            }
        }
    }

    public function save($pizza)
    {
        $array = $this->hardPizzas;
        array_push($d, $pizza);
        $this->setHardPizzas($array);
    }

    public function deleteById($pizzaId)
    {
    }

    public function update($pizzaId, $data)
    {
    }
}
