<?php

namespace App\Repositories\Pizza;

interface IPizzaRepository
{
    public function all();
    public function findById($pizzaId);
    public function save($pizza);
    public function deleteById($pizzaId);
    public function update($pizzaId, $data);
}
