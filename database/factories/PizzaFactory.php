<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pizza;
use Faker\Generator as Faker;

$factory->define(Pizza::class, function (Faker $faker) {
    return [
        'name' => $faker->firstNameMale,
        'type' => $faker->randomElement(
            [
                "Moozzarella ",
                "Napolitana", 
                "Especial", 
                "Anchoas"
            ]
            ),
        'base' =>  $faker->randomElement(
            [
                "Salsa de tomate ",
                
            ]
            ),
        'price' => $faker->randomDigit,
        'toppings' => [
            $faker->randomElement(
                    [
                    "musshrooms ",
                    "peppers", 
                    "garlic", 
                    "olives"
                    ]
                )
            ],
    ];
});
