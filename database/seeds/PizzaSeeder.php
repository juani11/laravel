<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SOLO UN SEEDER
       /*  $faker = Faker\Factory::create();
        DB::table('pizzas')->insert([
            'name' => $faker->firstNameMale,
            'type' => $faker->randomElement(
                [
                  "Moozzarella ",
                  "Napolitana", 
                  "Especial", 
                  "Anchoas"
                ]
             ),
            'base' =>  $faker->randomElement(
                [
                  "Salsa de tomate ",
                  
                ]
             ),
            'price' => $faker->randomDigit,
            'toppings' => json_encode([
                $faker->randomElement(
                     [
                       "musshrooms ",
                       "peppers", 
                       "garlic", 
                       "olives"
                     ]
                  )
             ]),
        ]); */


        //SEEDER USANDO FACTORY
        factory(App\Pizza::class, 12)->create();
    }
}
