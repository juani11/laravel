@extends('layouts.layout')

@section('content')



<div class="wrapper create-pizza">
  <h1>Create a New Pizza</h1>
  <form action="/pizzas" method="POST">
    @csrf
    <label for="name">Your name:</label>
    <input type="text" name="name" id="name">
    <label for="type">Choose type of pizza:</label>
    <select name="type" id="type">
      <option value="margarita">Margarita</option>
      <option value="hawaiian">Hawaiian</option>
      <option value="veg supreme">Veg Supreme</option>
      <option value="volcano">Volcano</option>
    </select>
    <label for="base">Choose crust:</label>
    <select name="base" id="base">
      <option value="thick">Thick</option>
      <option value="thin & crispy">Thin & Crispy</option>
      <option value="cheese crust">Cheese Crust</option>
      <option value="garlic crust">Garlic Crust</option>
    </select>
    <label for="name">Price:</label>
    <input type="number" name="price" id="price" />
    <fieldset>
      <label for="toppings">Extra toppings</label>
      <input type="checkbox" name="toppings[]" value="mushrooms">Musshrooms</input>
      <input type="checkbox" name="toppings[]" value="peppers">Peppers</input>
      <input type="checkbox" name="toppings[]" value="garlic">Garlic</input>
      <input type="checkbox" name="toppings[]" value="olives">Olives</input>
    </fieldset>
    <input type="submit" value="Order Pizza">
  </form>
  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li style="color: red;">{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
</div>
@endsection