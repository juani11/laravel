@extends('layouts.layout')

@section('content')

<div class="wrapper create-pizza">
    <h1>Edit a Pizza</h1>
    <form action="/pizzas/{{$pizza->id}}" method="POST">
        @csrf
        @method('PUT')
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" value={{$pizza->name}} required>
        <label for="type">Type pizza:</label>
        <select name="type" id="type" value={{$pizza->type}}>
            <option value="margarita">Margarita</option>
            <option value="hawaiian">Hawaiian</option>
            <option value="veg supreme">Veg Supreme</option>
            <option value="volcano">Volcano</option>
            <option value="Moozzarella ">Moozzarella</option>

        </select>
        <label for="base">Choose crust:</label>
        <select name="base" id="base" value={{$pizza->base}}>
            <option value="thick">Thick</option>
            <option value="thin & crispy">Thin & Crispy</option>
            <option value="cheese crust">Cheese Crust</option>
            <option value="garlic crust">Garlic Crust</option>
            <option value="Salsa de tomate">Salsa de tomate</option>
        </select>
        <label for="name">Price:</label>
        <input type="number" name="price" id="price" value={{$pizza->price}} />
        <fieldset>
            <label for="toppings">Extra toppings</label>
            <input type="checkbox" name="toppings[]" value="mushrooms">Musshrooms</input>
            <input type="checkbox" name="toppings[]" value="peppers">Peppers</input>
            <input type="checkbox" name="toppings[]" value="garlic">Garlic</input>
            <input type="checkbox" name="toppings[]" value="olives">Olives</input>
        </fieldset>
        <input type="submit" value="Editar orden">
    </form>
</div>
@endsection