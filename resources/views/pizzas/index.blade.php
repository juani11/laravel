@extends('layouts.layout')

@section('content')
<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title m-b-md">
            Pizza Orders
        </div>
        @foreach ($pizzas as $pizza)
        <p><a href="/pizzas/{{$pizza->id}}">{{$pizza->name}} - {{$pizza->type}} - ${{$pizza->price}} </a> <a href="/pizzas/edit/{{$pizza->id}}">Editar</a></p>
        @endforeach

        <form action="/pizzas/create">
            <input type="submit" value="Crear orden" />
        </form>
    </div>
</div>
@endsection